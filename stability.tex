\documentclass[12pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper,margin=1in}                   		% ... or a4paper or a5paper or ... 
%\geometry{landscape}                		% Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% \hat{u}se pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amsmath, amsthm,amssymb}

\title{STABILITY CONDITION}
\author{Huy Le}
%\date{}							% Activate to display a given date or no date

\begin{document}
\maketitle
I derive the stability condition for finite difference solution to a simple wave equation with second order in time and $2m$ order in space. Let's start first with one dimension:
\begin{equation}
\frac{\partial u}{\partial t^2}=v^2\frac{\partial u}{\partial x^2}.
\end{equation}

When $m=1$, the discrete form is:
\begin{equation}
\frac{u(x,t+\triangle t)-2u(x,t)+u(x,t-\triangle t)}{\triangle t^2}=v^2\frac{u(x+\triangle x,t)-2u(x,t)+u(x-\triangle x,t)}{\triangle x^2}.
\end{equation}

Fourier transform the above equation in space:
\begin{equation}
\frac{\hat{u}(k_x,t+\triangle t)-2\hat{u}(k_x,t)+\hat{u}(k_x,t-\triangle t)}{\triangle t^2}=v^2\frac{e^{ik_x\triangle x}\hat{u}(k_x,t)-2\hat{u}(k_x,t)+e^{-ik_x\triangle x}\hat{u}(k_x,t)}{\triangle x^2}.
\end{equation}

Denoting $\hat{u}(k_x,t)=\hat{u}_n$, the above equation can be rewritten, after some manipulations, as:
\begin{equation}
\hat{u}_{n+1}=2\left\{\frac{v^2\triangle t^2}{\triangle x^2}\left[\cos(k_x\triangle x)-1\right]+1\right\}\hat{u}_n-\hat{u}_{n-1},
\end{equation}
or:
\begin{equation}
\hat{u}_{n+1}=a\hat{u}_n-\hat{u}_{n-1},
\label{eq:recursive}
\end{equation}
with $a=2\left\{\frac{v^2\triangle t^2}{\triangle x^2}\left[\cos(k_x\triangle x)-1\right]+1\right\}$. 

In the general case of $m$, denote the coefficients as $a_i$ with $i=-m...m$:
\begin{equation}
\begin{gathered}
a_{-i}=a_i,\\
a_i\begin{cases} > 0 \mbox{ if $i$ is odd}, \\ < 0 \mbox{ if $i$ is even}, \end{cases}\\
\sum_{i=-m}^m a_i=0.
\end{gathered}
\end{equation}
Now:
\begin{equation}
a=2\left\{\frac{v^2\triangle t^2}{\triangle x^2}\sum_{i=1}^m a_i \left[\cos(ik_x\triangle x)-1\right]+1\right\}.
\end{equation}

Plotting $a$ as a function of $k_x\triangle x$ for several cases of $m$, I notice that $a$ achieves its maximum when $k_x\triangle x=0$ and its minimum when $k_x\triangle x=\pi$. So:
\begin{equation}
2\left\{1-2\frac{v^2\triangle t^2}{\triangle x^2}\sum_{i \mbox{ odd}}^{i>0} a_i\right\} \leq a \leq 2.
\end{equation}

For $n$ dimensions, assuming the same discretization:
\begin{equation}
a=2\left\{\frac{v^2\triangle t^2}{\triangle x^2}\sum_{i=1}^m a_i \sum_{j=1}^n \left[\cos(ik_j\triangle x_j)-1\right]+1\right\},
\end{equation}
and therefore:
\begin{equation}
2\left\{1-2n\frac{v^2\triangle t^2}{\triangle x^2}\sum_{i \mbox{ odd}}^{i>0} a_i\right\} \leq a \leq 2.
\label{eq:a1}
\end{equation}

Equation \ref{eq:recursive} is a two-step recursive relation. In order to make it an one-step relation, combine it with the identity:
\begin{equation}
\hat{u}_n=\hat{u}_n,
\end{equation} 
and rewrite in matrix form:
\begin{equation}
\begin{bmatrix}  \hat{u}_{n+1} \\ \hat{u}_n \end{bmatrix}=\begin{bmatrix}  a & -1 \\ 1 & 0 \end{bmatrix}\begin{bmatrix}  \hat{u}_n \\ \hat{u}_{n-1} \end{bmatrix},
\end{equation}
or:
\begin{equation}
U_{n+1}=\mathbf{A}U_n,
\end{equation}
with $U_n=\begin{bmatrix}  \hat{u}_n \\ \hat{u}_{n-1} \end{bmatrix}$ and $\mathbf{A}=\begin{bmatrix}  a & -1 \\ 1 & 0 \end{bmatrix}$.

The matrix $\mathbf{A}$ is called the amplification matrix, whose eigenvalues have to have magnitude less than or equal to one for stability. Eigenvalues $\lambda$'s of $\mathbf{A}$ are roots of the quadratic equation:
\begin{equation}
\det(\mathbf{A}-\lambda\mathbf{I})=0,
\end{equation}
or:
\begin{equation}
\lambda^2-a\lambda+1=0.
\label{eq:eigen}
\end{equation}
Notice that the product of two roots of the above equation is one. As a result, if these roots are real, they both have to be equal to one, because otherwise one of them will have magnitude greater than one. However, two roots are equal to one only if $a=2$ $\forall k$, which is impossible. So, for stability, equation \ref{eq:eigen} has to have complex roots, which mean:
\begin{equation}
a^2\leq4,
\end{equation}
or
\begin{equation}
-2\leq a\leq2.
\label{eq:a2}
\end{equation}

From equations \ref{eq:a1} and \ref{eq:a2}, the stability condition is:
\begin{equation}
2\left\{1-2n\frac{v^2\triangle t^2}{\triangle x^2}\sum_{i \mbox{ odd}}^{i>0} a_i\right\} \geq -2,
\end{equation}
or:
\begin{displaymath}
\frac{v\triangle t}{\triangle x} \leq \frac{1}{\sqrt{n\sum\limits_{i \mbox{ odd}}^{i>0} a_i}}.
\end{displaymath}\

\end{document}  